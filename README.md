# Physics Simulation Coursework
## Game Mechanics
### Objective
Kill the dragon by shooting it with arrows

You can use the pool to heal low health raiders

The treasure hoard gives a damage buff to raiders that reach it

Rubble slows your movement speed

### Video Demo

https://www.youtube.com/watch?v=_6nrckkBTjg&feature=youtu.be

### Controls
* Up - Move leader forward
* Left - Rotate leader left
* Right - Rotate leader right
* Space - Shoot arrows at the dragon
* K - Spawn raiders
* J - Send the lowest health raider to the pool
* L - Send a random raider to the treasure hoard
* B - Toggle display of the navigation grid
## Build Information
The solution file is in the **CSC3222 Coursework** folder

It is currently only configured for Win32

Startup project should be set to CSC3222 Coursework Framework

Build directories are in **CSC3222 Coursework** then either **Release** or **Debug**
