#include "../../nclgl/window.h"
#include "Renderer.h"
#include "Physics.h"

#pragma comment(lib, "nclgl.lib")

int main() {
	Window w("CSC3222 Coursework", 1920, 1080, false);
	//Window w("CSC3222 Coursework", 1920, 1380, false);
	if (!w.HasInitialised()) {
		return -1;
	}

	Physics physics;
	Renderer renderer(w, &physics);
	
	if (!renderer.HasInitialised()) {
		return -1;
	}

	w.LockMouseToWindow(false);
	w.ShowOSPointer(true);

	while (w.UpdateWindow() && !Window::GetKeyboard()->KeyDown(KEYBOARD_ESCAPE)) {
		float msec = w.GetTimer()->GetTimedMS();

		physics.UpdatePhysics(msec, Window::GetKeyboard());
		renderer.UpdateScene(msec);
		renderer.RenderScene();

		if (Window::GetKeyboard()->KeyTriggered(KEYBOARD_P)) {
			int milli = w.GetTimer()->GetMS();
			while(true) {
				int s = w.GetTimer()->GetMS() - milli;
				if (s > 5000) {
					break;
				}
			}
		}
	}

	return 0;
}