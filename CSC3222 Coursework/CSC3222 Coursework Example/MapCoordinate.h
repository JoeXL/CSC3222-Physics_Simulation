#pragma once
#include "../../nclgl/Vector3.h"

class MapCoordinate
{
public:
	MapCoordinate(int xCoord, int yCoord, int zCoord);
	~MapCoordinate();

	float getX() { return x; }
	float getY() { return y; }
	float getZ() { return z; }

	Vector3 getVector() { return Vector3(x, y, z); }

private:
	float x;
	float y;
	float z;
	float multiplier = 27.0f;
};

