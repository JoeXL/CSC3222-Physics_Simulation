#pragma once
#include "Entity.h"

class Raider : public Entity
{
public:
	Raider();
	~Raider();

	const int& getHealth() const { return health; }
	void setHealth(int hp) { health = hp; }

	const int& getMaxHealth() const { return maxHealth; }
	void setMaxHealth(int hp) { maxHealth = hp; }

	const bool& isAlive() const { return alive; }
	void setAlive(bool b) { alive = b; }

private:
	int health;
	int maxHealth;

	bool alive = false;
};

