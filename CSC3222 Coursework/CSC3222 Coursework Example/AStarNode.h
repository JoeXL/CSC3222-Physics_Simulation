#pragma once
#include <string>
#include <vector>
#include "../../nclgl/Vector3.h"

using namespace std;

enum TerrainType {
	RUBBLE, WALL, BOULDER, GROUND, HOARD, POOL, ENTRANCE
};

class AStarNode
{
public:
	AStarNode();
	~AStarNode();

	const int& getXId() const { return xId; }
	void setXId(int x) { xId = x; }

	const int& getYId() const { return yId; }
	void setYId(int y) { yId = y; }

	const TerrainType& getTerrainType() const { return type; }
	void setTerrainType(TerrainType t) { type = t; }

	const Vector3& getPosition() const { return position; }
	void setPosition(Vector3 pos) { position = pos; }

	const vector<AStarNode*>& getNeighbours() const{ return neighbours; }
	void addNeighbour(AStarNode* node) { neighbours.push_back(node); }

	bool operator==(const AStarNode& other);

private:
	int xId;
	int yId;
	Vector3 position;
	vector<AStarNode*> neighbours;
	TerrainType type;
};

