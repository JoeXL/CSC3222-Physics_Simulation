#pragma once
#include "Raider.h"
#include "AStarNode.h"

enum StateFollower {
	FOLLOWER_WAIT, FOLLOWER_FOLLOW_LEADER, FOLLOWER_PANIC, FOLLOWER_DEAD, FOLLOWER_TO_POOL, FOLLOWER_FROM_POOL, FOLLOWER_TO_HOARD, FOLLOWER_FROM_HOARD
};

class Follower : public Raider
{
public:
	Follower();
	~Follower();

	const StateFollower& getState() const { return state; }
	void setState(StateFollower s) { state = s; }

	vector<AStarNode*> getPath() { return path; }
	void setPath(vector<AStarNode*> newPath) { path = newPath; }

	int getTargetNode() { return targetNode; }
	void setTargetNode(int node) { targetNode = node; }

	bool getHasDamageBuff() { return hasDamageBuff; }
	void setHasDamageBuff(bool b) { hasDamageBuff = b; }

private:
	StateFollower state;
	vector<AStarNode*> path;
	int targetNode = 0;

	bool hasDamageBuff = false;
};

