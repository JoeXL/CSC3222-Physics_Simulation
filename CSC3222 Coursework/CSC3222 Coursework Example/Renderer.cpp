#include "Renderer.h"

Renderer::Renderer(Window &parent, Physics* physics) : OGLRenderer(parent) {

	p = physics;

	/* We have extended the SceneNode class to include a pointer to a SceneNode's PhysicsData.
	We have then extended the SceneNode's Update function to update to the latest position and 	rotation values, which are
	updated in Physics::UpdatePhysics. */

	projMatrix = Matrix4::Orthographic(-1.0f, 10000.0f, width / 2.0f, -width / 2.0f, height / 2.0f, -height / 2.0f);
	
	camera = new Camera(0.0f, 0.0f, Vector3(0, 100, 750.0f));

	currentShader = new Shader(SHADERDIR"SceneVertex.glsl", SHADERDIR"SceneFragment.glsl");

	// The Map

	map = Mesh::GenerateQuad();

	//map->SetTexture(SOIL_load_OGL_texture(TEXTUREDIR"collisionMapUpdated.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, 0));
	map->SetTexture(SOIL_load_OGL_texture(TEXTUREDIR"map.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, 0));

	// The collision map
	int width = 2100;
	int height = 1350;
	physics->collisionMapWidth = width;
	physics->collisionMapHeight = height;
	GLuint colMap = SOIL_load_OGL_texture(TEXTUREDIR"collisionMapUpdated.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, 0);
	glBindTexture(GL_TEXTURE_2D, colMap);
	float* texData = new float[width * height * 3];
	glGetTexImage(GL_TEXTURE_2D, 0, GL_RGB, GL_FLOAT, texData);
	glDeleteTextures(1, &colMap);

	int rows = 0;
	int column = 0;
	for (int i = 0; i < (width * height * 3); i += 3) {

		Vector3 rgb(texData[i], texData[i + 1], texData[i + 2]);

		if (column == 0) {
			vector<Vector3> row;
			physics->collisionMap.push_back(row);
		}

		physics->collisionMap.at(rows).push_back(rgb);
		column++;
		if (column == width) {
			column = 0;
			rows++;
		}
	}

	delete texData;

	p->aStarMap.generateMap(p->collisionMap, width, height);

	if (!currentShader->LinkProgram() || !map->GetTexture()) {
		return;
	}

	endScreen = Mesh::GenerateQuad();
	victoryTex = SOIL_load_OGL_texture(TEXTUREDIR"victory.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, 0);
	failureTex = SOIL_load_OGL_texture(TEXTUREDIR"failure.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, 0);

	if (!victoryTex || !failureTex) {
		return;
	}

	// The dragon

	dragon = Mesh::GenerateQuad();

	dragon->SetTexture(SOIL_load_OGL_texture(TEXTUREDIR"dragon.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, 0));
	dragonDead = SOIL_load_OGL_texture(TEXTUREDIR"dragonDead.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, 0);

	if (!currentShader->LinkProgram() || !dragon->GetTexture() || !dragonDead)
	{
		return;
	}

	// The dragon's breath weapon

	breathWeapon = Mesh::GenerateQuad();

	breathWeapon->SetTexture(SOIL_load_OGL_texture(TEXTUREDIR"breath.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, 0));

	if (!currentShader->LinkProgram() || !breathWeapon->GetTexture())
	{
		return;
	}


	// Raider graphics - feel free to extend and edit

	raider = Mesh::GenerateQuad();

	raider->SetTexture(SOIL_load_OGL_texture(TEXTUREDIR"raiderRotated.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, 0));

	leader = Mesh::GenerateQuad();

	leader->SetTexture(SOIL_load_OGL_texture(TEXTUREDIR"raiderLeader.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, 0));

	if (!currentShader->LinkProgram() || !raider->GetTexture()) {
		return;
	}

	// Arrow graphics
	arrow = Mesh::GenerateQuad();
	arrow->SetTexture(SOIL_load_OGL_texture(TEXTUREDIR"arrow.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, 0));

	if (!currentShader->LinkProgram() || !arrow->GetTexture()) {
		return;
	}

	nodeBlockedMesh = Mesh::GenerateQuad();
	nodeBlockedMesh->SetTexture(SOIL_load_OGL_texture(TEXTUREDIR"nodeBlocked.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, 0));
	nodeEntraceMesh = Mesh::GenerateQuad();
	nodeEntraceMesh->SetTexture(SOIL_load_OGL_texture(TEXTUREDIR"nodeEntrance.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, 0));
	nodeHoardMesh = Mesh::GenerateQuad();
	nodeHoardMesh->SetTexture(SOIL_load_OGL_texture(TEXTUREDIR"nodeHoard.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, 0));
	nodePassableMesh = Mesh::GenerateQuad();
	nodePassableMesh->SetTexture(SOIL_load_OGL_texture(TEXTUREDIR"nodePassable.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, 0));
	nodePoolMesh = Mesh::GenerateQuad();
	nodePoolMesh->SetTexture(SOIL_load_OGL_texture(TEXTUREDIR"nodePool.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, 0));
	nodeSlowerMesh = Mesh::GenerateQuad();
	nodeSlowerMesh->SetTexture(SOIL_load_OGL_texture(TEXTUREDIR"nodeSlower.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, 0));

	/* Declaring our SceneNodes places objects in the environment. We begin with a root note. It you look at the
	constructor, you'll see it defaults to position 0,0,0 - as such, all SceneNodes which are children of the root note are
	translated relative to 0,0,0. This framework means we don't really have to worry about this overmuch - so long as our
	updated PhysicsData makes sense, objects should appear where we want them to appear, and pointing in the right
	direction. */

	root = new SceneNode();

	for (int i = 0; i < p->aStarMap.nodes.size(); ++i) {
		for (int j = 0; j < p->aStarMap.nodes.at(i).size(); ++j) {
			Entity* n = new Entity;
			n->physicsNode.updatePosition(p->aStarMap.nodes.at(i).at(j)->getPosition());
			n->physicsNode.updateRotation(0);
			n->physicsNode.updateScale(Vector3(0, 0, 0));
			p->nodes.push_back(n);

			SceneNode* starNode = new SceneNode();
			starNode->SetColour(Vector4(1, 1, 1, 0.99));
			starNode->SetTransform(
				Matrix4::Translation(p->aStarMap.nodes.at(i).at(j)->getPosition())
				* Matrix4::Rotation(0, Vector3(0,1,0)));
			starNode->SetModelScale(Vector3(1, 1, 1));
			TerrainType t = p->aStarMap.nodes.at(i).at(j)->getTerrainType();
			switch (t) {
			case RUBBLE: {
				starNode->SetMesh(nodeSlowerMesh);
				break;
			}
			case WALL: {
				starNode->SetMesh(nodeBlockedMesh);
				break;
			}
			case BOULDER: {
				starNode->SetMesh(nodeBlockedMesh);
				break;
			}
			case GROUND: {
				starNode->SetMesh(nodePassableMesh);
				break;
			}
			case HOARD: {
				starNode->SetMesh(nodeHoardMesh);
				break;
			}
			case POOL: {
				starNode->SetMesh(nodePoolMesh);
				break;
			}
			case ENTRANCE: {
				starNode->SetMesh(nodeEntraceMesh);
				break;
			}
			}
			starNode->SetBoundingRadius(5.0f);
			starNode->SetEntity(n);
			root->AddChild(starNode);
		}
	}

	SceneNode * mapNode = new SceneNode();
	mapNode->SetColour(Vector4(1, 1, 1, 1));
	mapNode->SetTransform(
		Matrix4::Translation(p->map.physicsNode.getPosition())
		* Matrix4::Rotation(p->map.physicsNode.getRotation(), Vector3(0, 0, 1)));
	mapNode->SetModelScale(p->map.physicsNode.getScale());
	mapNode->SetMesh(map);
	mapNode->SetBoundingRadius(5.0f);
	mapNode->SetEntity(&(p->map));
	root->AddChild(mapNode);

	SceneNode* endScreenNode = new SceneNode();
	endScreenNode->SetColour(Vector4(1, 1, 1, 0.99));
	endScreenNode->SetTransform(
		Matrix4::Translation(p->endScreen.physicsNode.getPosition())
		* Matrix4::Rotation(p->endScreen.physicsNode.getRotation(), Vector3(0, 0, 1)));
	endScreenNode->SetModelScale(p->endScreen.physicsNode.getScale());
	endScreenNode->SetMesh(endScreen);
	endScreenNode->SetBoundingRadius(5.0f);
	endScreenNode->SetEntity(&(p->endScreen));
	root->AddChild(endScreenNode);

	SceneNode * dragonNode = new SceneNode();
	dragonNode->SetColour(Vector4(1, 1, 1, 0.99));
	dragonNode->SetTransform(
		Matrix4::Translation(p->dragon.physicsNode.getPosition()) 
		* Matrix4::Rotation(p->dragon.physicsNode.getRotation(), Vector3(0, 0, 1)));
	dragonNode->SetModelScale(p->dragon.physicsNode.getScale());
	dragonNode->SetMesh(dragon);
	dragonNode->SetBoundingRadius(5.0f);
	dragonNode->SetEntity(&(p->dragon));
	root->AddChild(dragonNode);

	/* Note that breathNode below is an example of a child of dragonNode. This means that its position is translated relative
	to the dragon (notice how 'out of sync' its coordinates appear to be in Physics::Physics() - only slightly moved down
	(negative y). Also notice how it moves when the dragon does, without being explicitly told to. Its rotation is initially set purely
	to make sure it points down. Note that if we rotate the dragon (you can experiment with that) the breathNode moves as though it were
	bolted to the bottom of the dragon. */

	SceneNode* breathNode = new SceneNode();
	breathNode->SetColour(Vector4(1, 1, 1, 0.999));
	breathNode->SetTransform(
		Matrix4::Translation(p->breath.physicsNode.getPosition())
		* Matrix4::Rotation(p->breath.physicsNode.getRotation(), Vector3(0, 0, 1)));
	breathNode->SetModelScale(p->breath.physicsNode.getScale());
	breathNode->SetMesh(breathWeapon);
	breathNode->SetBoundingRadius(5.0f);
	breathNode->SetEntity(&(p->breath));
	dragonNode->AddChild(breathNode);

	for (int i = 0; i < p->raiders.size(); ++i) {
		SceneNode * s = new SceneNode();
		s->SetColour(Vector4(1, 1, 1, 0.999));
		s->SetTransform(Matrix4::Translation(p->raiders.at(i)->physicsNode.getPosition())
			*Matrix4::Rotation(p->raiders.at(i)->physicsNode.getRotation(), Vector3(0,0,1)));
		s->SetModelScale(p->raiders.at(i)->physicsNode.getScale());
		if (i == 0) {
			s->SetMesh(leader);
		}
		else {
			s->SetMesh(raider);
		}
		s->SetBoundingRadius(5.0f);
		s->SetEntity(p->raiders.at(i));
		root->AddChild(s);
	}

	for (int i = 0; i < p->arrows.size(); ++i) {
		SceneNode * s = new SceneNode();
		s->SetColour(Vector4(1, 1, 1, 0.999));
		s->SetTransform(Matrix4::Translation(p->arrows.at(i)->physicsNode.getPosition())
			*Matrix4::Rotation(p->arrows.at(i)->physicsNode.getRotation(), Vector3(0, 0, 1)));
		s->SetModelScale(p->arrows.at(i)->physicsNode.getScale());
		s->SetMesh(arrow);
		s->SetBoundingRadius(5.0f);
		s->SetEntity(p->arrows.at(i));
		root->AddChild(s);
	}

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	init = true;
}

Renderer::~Renderer(void) {
	delete root;
	delete raider;
	delete dragon;
	delete breathWeapon;
	delete map;
	delete camera;
}

void Renderer::UpdateScene(float msec) {
	viewMatrix = camera->BuildViewMatrix();
	frameFrustum.FromMatrix(projMatrix*viewMatrix);

	int raidersAlive = 0;
	Leader* leader = (Leader*)p->raiders.at(0);
	if (leader->getState() == LEADER_DEAD) {
		root->RemoveChild(leader);
	}
	else {
		++raidersAlive;
	}
	for (vector<Raider*>::iterator i = p->raiders.begin() + 1; i != p->raiders.end(); ++i) {
		Follower* follower = (Follower*)(*i);
		if (follower->getState() == FOLLOWER_DEAD) { // If the raider is dead, remove it from the root scenenode
			root->RemoveChild(follower);
		}
		else {
			++raidersAlive;
		}
	}
	if (raidersAlive == 0) { // If all raiders are dead display the failure image
		endScreen->SetTexture(failureTex);
	}
	if (p->dragon.getState() == DRAGON_DEAD) { // If the dragon is dead display the victory image
		root->GetChild(&p->dragon)->GetMesh()->SetTexture(dragonDead);
		endScreen->SetTexture(victoryTex);
	}

	root->Update(msec);
}

void Renderer::RenderScene() {
	BuildNodeLists(root);
	SortNodeLists();

	glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

	glUseProgram(currentShader->GetProgram());
	UpdateShaderMatrices();

	glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "diffuseTex"), 0);

	DrawNodes();

	glUseProgram(0);
	SwapBuffers();

	ClearNodeLists();
}

void	Renderer::DrawNode(SceneNode*n) {
	if (n->GetMesh()) {
		glUniformMatrix4fv(glGetUniformLocation(currentShader->GetProgram(), "modelMatrix"), 1, false, (float*)&(n->GetWorldTransform()*Matrix4::Scale(n->GetModelScale())));
		glUniform4fv(glGetUniformLocation(currentShader->GetProgram(), "nodeColour"), 1, (float*)&n->GetColour());

		glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "useTexture"), (int)n->GetMesh()->GetTexture());
		GL_BREAKPOINT

			n->GetMesh()->Draw();
	}
}

void	Renderer::BuildNodeLists(SceneNode* from) {
	Vector3 direction = from->GetWorldTransform().GetPositionVector() - camera->GetPosition();
	from->SetCameraDistance(Vector3::Dot(direction, direction));

	if (frameFrustum.InsideFrustum(*from)) {
		if (from->GetColour().w < 1.0f) {
			transparentNodeList.push_back(from);
		}
		else {
			nodeList.push_back(from);
		}
	}

	for (vector<SceneNode*>::const_iterator i = from->GetChildIteratorStart(); i != from->GetChildIteratorEnd(); ++i) {
		BuildNodeLists((*i));
	}
}

void	Renderer::DrawNodes() {
	for (vector<SceneNode*>::const_iterator i = nodeList.begin(); i != nodeList.end(); ++i) {
		DrawNode((*i));
	}

	for (vector<SceneNode*>::const_reverse_iterator i = transparentNodeList.rbegin(); i != transparentNodeList.rend(); ++i) {
		DrawNode((*i));
	}
}

void	Renderer::SortNodeLists() {
	std::sort(transparentNodeList.begin(), transparentNodeList.end(), SceneNode::CompareByCameraDistance);
	std::sort(nodeList.begin(), nodeList.end(), SceneNode::CompareByCameraDistance);
}

void	Renderer::ClearNodeLists() {
	transparentNodeList.clear();
	nodeList.clear();
}