#pragma once

#include "../../nclgl/Vector3.h"
#include "MapCoordinate.h"

class PhysicsNode
{
public:
	PhysicsNode();
	~PhysicsNode();

	void updatePosition(Vector3 pos) { position = pos; }
	void updatePositionNoZ(Vector3 pos) { position.x = pos.x; position.y = pos.y; }
	void updatePosition(MapCoordinate coord) { position = coord.getVector(); }
	void updatePositionX(float x) { position.x = x; }
	void updatePositionY(float y) { position.y = y; }
	void updatePositionZ(float z) { position.z = z; }

	void updateRotation(float rot) {
		while (rot > 360) {
			rot = rot - 360;
		}
		rotation = rot;
	}

	void updateScale(Vector3 scl) { scale = scl; }
	void updateScaleX(float x) { scale.x = x; }
	void updateScaleY(float y) { scale.y = y; }
	void updateScaleZ(float z) { scale.z = z; }

	void updateAggroRange(float x) { aggroRange = x; }
	void updateCollisionRadius(float x) { collisionRadius = x; }

	void updateTargetVelocity(float x) { targetSpeed = x; }
	void updateCurrentVelocity(Vector3 x) { currentVelocity = x; }
	void updateCurrentAcceleration(Vector3 x) { currentAcceleration = x; }

	void updateMass(float m) { mass = m; }

	const Vector3& getXYPosition() const { return Vector3(position.x, position.y, 0); }
	const Vector3& getPosition() const { return position; }

	const Vector3& getNextXYPosition(float msec) const { return Vector3(position.x, position.y, 0) - currentVelocity * msec; }
	const Vector3& getNextPosition(float msec) const { return position - currentVelocity * msec; }

	const float& getRotation() const { return rotation; }
	const Vector3& getScale() const { return scale; }
	const float& getAggroRange() const { return aggroRange; }
	const float& getCollisionRadius() const { return collisionRadius; }
	const float& getTargetSpeed() const { return targetSpeed; }
	const Vector3& getCurrentVelocity() const { return currentVelocity; }
	const Vector3& getCurrentAcceleration() const { return currentAcceleration; }
	const float& getMass() const { return mass; }

private:
	Vector3 position;
	float rotation;
	Vector3 scale;
	float aggroRange;
	float collisionRadius;
	float targetSpeed;
	Vector3 currentVelocity;
	Vector3 currentAcceleration;
	float mass = 0;
};

