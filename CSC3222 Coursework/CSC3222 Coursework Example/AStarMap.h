#pragma once
#include "AStarNode.h"
#include "MapCoordinate.h"
#include "../../nclgl/Vector2.h"
#include <map>

class AStarMap
{
public:
	AStarMap();
	~AStarMap();

	void generateMap(vector<vector<Vector3>> collisionMap, int width, int height);

	AStarNode* getNode(int x, int y) {
		for (int i = 0; i < nodes.size(); ++i) {
			for(int j = 0; j < nodes.at(i).size(); ++j) {
				if (nodes.at(i).at(j)->getXId() == x && nodes.at(i).at(j)->getYId() == y) {
					return nodes.at(i).at(j);
				}
			}
		}
		return nullptr;
	}

	void print() {
		for (int i = 0; i < nodesWidth; ++i) {
			for (int j = 0; j < nodesHeight; ++j) {
				cout << getNode(i, j)->getXId();
			}
		}
		cout << endl;
	}

	vector<AStarNode*> calculatePath(Vector2 startPosition, Vector2 endPosition);

	vector<vector<AStarNode*>> nodes;
	float rubbleMultiplier = 1;

protected:
	AStarNode* findClosestPathableNode(Vector2 position);

	vector<AStarNode*> reconstructPath(map<AStarNode*, AStarNode*> cameFrom, AStarNode* current);

	float estimateHeuristic(AStarNode* start, AStarNode* end);

	float calcCost(AStarNode* current, AStarNode* neighbour);

	int nodesWidth = 60;
	int nodesHeight = 40;
};

