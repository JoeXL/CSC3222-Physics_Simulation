#pragma once
#include "Raider.h"

enum StateLeader {
	LEADER_WAIT, LEADER_ALIVE, LEADER_DEAD
};

class Leader : public Raider
{
public:
	Leader();
	~Leader();

	const StateLeader& getState() const { return state; }
	void setState(StateLeader s) { state = s; }

private:
	StateLeader state;
};

