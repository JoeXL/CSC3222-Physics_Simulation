#pragma once

#include <vector>
#include "../../nclgl/Vector3.h"
#include "PhysicsNode.h"

class Entity
{
public:
	Entity();
	~Entity();

	PhysicsNode physicsNode;
};