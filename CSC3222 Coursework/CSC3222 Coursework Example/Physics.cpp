#include "Physics.h"

Physics::Physics()
{
	srand(time(NULL));
	numRaiders = 16;
	/* Steup leader values */
	Leader* lead = new Leader();
	lead->physicsNode.updatePosition(MapCoordinate(19, 12, 2));
	lead->physicsNode.updateScale(Vector3(0, 0, 0));
	lead->physicsNode.updateRotation(0);
	lead->physicsNode.updateCollisionRadius(10);
	lead->physicsNode.updateMass(9999);
	lead->physicsNode.updateTargetVelocity(groundSpeedModifier);
	lead->setHealth(leaderHealth);
	lead->setMaxHealth(leaderMaxHealth);
	lead->setAlive(false);
	lead->setState(LEADER_WAIT);
	raiders.push_back(lead);

	/* Setup follower values */
	for (int i = 1; i <= numRaiders; i++)
	{
		Follower* temp = new Follower();

		temp->physicsNode.updatePosition(MapCoordinate(19 + i, 12, 2 + i));
		temp->physicsNode.updateRotation(i*20.0f);
		temp->physicsNode.updateScale(Vector3(0, 0, 0));
		temp->physicsNode.updateCollisionRadius(10);
		temp->physicsNode.updateTargetVelocity(groundSpeedModifier);
		temp->physicsNode.updateMass(1);
		temp->setAlive(false);

		temp->setState(FOLLOWER_WAIT);
		
		// Random health generation
		float r = (rand() % (followerHealthMax - followerHealthMin + 1)) + followerHealthMin;
		temp->setHealth(r);
		temp->setMaxHealth(followerHealthMax);

		raiders.push_back(temp);
	}

	/* Create an arrow for each follower */
	for (int i = 0; i < numRaiders; ++i) {
		Arrow* arrow = new Arrow();
		arrow->physicsNode.updatePosition(Vector3(0, 0, 0));
		arrow->physicsNode.updateScale(Vector3(0.0f, 0.0f, 0.0f));
		arrow->physicsNode.updateCollisionRadius(5);
		arrow->physicsNode.updateTargetVelocity(arrowSpeedModifier);
		arrow->setDamage(arrowDamage);
		arrow->setLifetime(arrowLifetime);

		arrows.push_back(arrow);
	}

	/* Setup map and end screen values */
	map.physicsNode.updatePosition(Vector3(0.0f, 100.0f, -200.0f));
	map.physicsNode.updateRotation(0.0f);
	map.physicsNode.updateScale(Vector3(864.0f, 540.0f, 100.0f));

	endScreen.physicsNode.updatePosition(Vector3(0.0f, 100.0f, 0));
	endScreen.physicsNode.updateRotation(0.0f);
	endScreen.physicsNode.updateScale(Vector3(0, 0, 0));

	/* Setup dragon and breath values */
	dragon.physicsNode.updatePosition(MapCoordinate(40, 24, 30));
	dragon.physicsNode.updatePositionY(dragon.physicsNode.getPosition().y + 9);
	dragon.physicsNode.updateRotation(0.0f);
	dragon.physicsNode.updateScale(Vector3(50.0f, 50.0f, 1.0f));
	dragon.physicsNode.updateCollisionRadius(50.0f);
	dragon.physicsNode.updateAggroRange(27.0f * dragonAggroRange);
	dragon.physicsNode.updateTargetVelocity(dragonSpeedModifier);
	dragon.physicsNode.updateMass(dragonMass);
	dragon.setHealth(dragonHealth);
	dragon.setMaxHealth(dragonMaxHealth);
	dragon.setAlive(true);
	dragon.setState(DRAGON_WAIT);

	dragonState = 1;

	breath.physicsNode.updatePosition(Vector3(0.0f, -50.0f, -2));
	breath.physicsNode.updateRotation(270.0f);
	breath.physicsNode.updateScale(Vector3(2.0f, 1.0f, 1.0f));

	breathState = 1;

	raidersAlive = numRaiders + 1;

}

Physics::~Physics()
{
	/* Free raider and arrow vectors */
	for (vector<Raider*>::iterator iterator = raiders.begin(); iterator != raiders.end(); ++iterator) {
		delete (*iterator);
	}
	raiders.clear();
	for (vector<Arrow*>::iterator iterator = arrows.begin(); iterator != arrows.end(); ++iterator) {
		delete (*iterator);
	}
	arrows.clear();
	for (vector<Entity*>::iterator iterator = nodes.begin(); iterator != nodes.end(); ++iterator) {
		delete (*iterator);
	}
	nodes.clear();
}

void Physics::UpdatePhysics(float msec, Keyboard* keyboard)
{
	this->msec = msec;

	/* Update all entities */
	updateDragon();
	updateFollowers();
	updateLeader();
	updateArrows();

	/* Process any keypresses */
	processKeys(keyboard);

	/* If an end game state is achieved, display the end screen entity */
	if (raidersAlive == 0 || dragon.getHealth() <= 0) {
		endScreen.physicsNode.updateScale(endScreenScale);
		if(raidersAlive == 0)
			dragon.setState(DRAGON_WAIT);
		gameStarted = false;
	}
}

const float Physics::angleBetweenPoints(Vector3 lhs, Vector3 rhs) const {
	if ((lhs.x == rhs.x) && (lhs.y == rhs.y))
		return 0;
	float angle = atan((lhs.x - rhs.x) / (lhs.y - rhs.y)) * 57.29577951f;
	if (lhs.y < rhs.y)
		angle = -angle;
	else
		angle = -angle + 180;

	return angle;
}

const bool Physics::overlap(Vector3 lhsPosition, float lhsRadius, Vector3 rhsPosition, float rhsRadius) const {

	if ((lhsPosition - rhsPosition).Length() < (lhsRadius + rhsRadius))
		return true;
	return false;
}

void Physics::processKeys(Keyboard* keyboard) {
	if (keyboard->KeyDown(KEYBOARD_LEFT))
		rotateLeaderLeft = true;
	if (keyboard->KeyDown(KEYBOARD_RIGHT))
		rotateLeaderRight = true;
	if (keyboard->KeyDown(KEYBOARD_UP))
		moveLeader = true;
	if (keyboard->KeyTriggered(KEYBOARD_SPACE))
		shootArrows = true;
	if (keyboard->KeyTriggered(KEYBOARD_B)) { // Toggle drawing of the AStar node map
		showAStarNodeMap = !showAStarNodeMap;
		Vector3 scale(0, 0, 0);
		if (showAStarNodeMap)
			scale = Vector3(10, 10, 10);
		for (vector<Entity*>::iterator nodeIt = nodes.begin(); nodeIt != nodes.end(); ++nodeIt) {
			(*nodeIt)->physicsNode.updateScale(scale);
		}
	}
	if (keyboard->KeyTriggered(KEYBOARD_K)) {
		gameStarted = true;
		spawnRaiders();
		dragon.setState(DRAGON_FOLLOW_LEADER);
		((Leader*)raiders.at(0))->setState(LEADER_ALIVE);
		for (vector<Raider*>::iterator i = raiders.begin() + 1; i != raiders.end(); ++i) {
			Follower* follower = (Follower*)(*i);
			follower->setState(FOLLOWER_FOLLOW_LEADER);
		}
	}
	if (keyboard->KeyTriggered(KEYBOARD_J)) {
		if (raidersAlive > 1 && ((Leader*)raiders.at(0))->getState() == LEADER_ALIVE) { // If the leader is alive and there are still raiders alive
			Follower* lowestFollower = (Follower*)raiders.at(1);
			float lowestHp = lowestFollower->getHealth();

			for (vector<Raider*>::iterator it = raiders.begin() + 1; it != raiders.end(); ++it) { // Find the lowest hp raider that is alive
				Follower* tempFollower = (Follower*)(*it);
				if (tempFollower->getHealth() < lowestHp && tempFollower->getState() != FOLLOWER_DEAD) {
					lowestFollower = tempFollower;
					lowestHp = tempFollower->getHealth();
				}
			}

			Vector2 startPos = Vector2(lowestFollower->physicsNode.getPosition().x, lowestFollower->physicsNode.getPosition().y);

			/* Set rubble multiplier and run the algrotihm */
			aStarMap.rubbleMultiplier = 1;
			vector<AStarNode*> path = aStarMap.calculatePath(startPos, poolPosition);

			if (path.size() != 0) { //If a path was found, set the follower state and path
				lowestFollower->setState(FOLLOWER_TO_POOL);
				lowestFollower->setPath(path);
				lowestFollower->setTargetNode(path.size() - 1);
			}
		}
	}
	if (keyboard->KeyTriggered(KEYBOARD_L)) {
		if (raidersAlive != 0 && ((Leader*)raiders.at(0))->getState() == LEADER_ALIVE) { // If the leader is alive and there are still raiders alive
			float raiderNum = 0;
			while (true) { // Find a random raider that is not dead
				raiderNum = (rand() % (raiders.size() - 1)) + 1;
				if (((Follower*)raiders[raiderNum])->getState() != FOLLOWER_DEAD)
					break;
			}
			Follower* follower = (Follower*)raiders[raiderNum];
			Vector2 startPos = Vector2(follower->physicsNode.getPosition().x, follower->physicsNode.getPosition().y);

			/* Set rubble multiplier and run the algrotihm */
			aStarMap.rubbleMultiplier = 500;
			vector<AStarNode*> path = aStarMap.calculatePath(startPos, hoardPosition);

			if (path.size() != 0) { //If a path was found, set the follower state and path
				follower->setState(FOLLOWER_TO_HOARD);
				follower->setPath(path);
				follower->setTargetNode(path.size() - 1);
			}
		}
	}
}

vector<Vector3> Physics::collision(Vector3 iVelocity, Vector3 iPosition, float iMass, Vector3 jVelocity, Vector3 jPosition, float jMass) {
	Vector3 vab = iVelocity - jVelocity;
	Vector3 N = iPosition - jPosition;
	N.Normalise();
	float JTop = Vector3::Dot(vab * -(1 + 0.9), N);
	float JBottom = Vector3::Dot(N, N * ((1 / iMass) + (1 / jMass)));
	float J = JTop / JBottom;
	Vector3 iNewVelocity = iVelocity + (N * (J / iMass));
	Vector3 jNewVelocity = jVelocity - (N * (J / jMass));
	vector<Vector3> collisions;
	collisions.push_back(iNewVelocity);
	collisions.push_back(jNewVelocity);
	return collisions;
}

Vector2 Physics::getCollisionMapCoord(float x, float y) {
	int texCoordx = (x + 864.0) / (1728.0 / (float)collisionMapWidth);
	int texCoordy = (-y + 640.0) / (1078.0 / (float)collisionMapHeight);
	return Vector2(texCoordx, texCoordy);
}

Vector3 Physics::getColourAtTexCoord(Vector2 texCoord) {
	/* If the texture coordinate is outside the viable range, just set it to 0,0 */
	if (texCoord.x < 0 || texCoord.x >= collisionMapWidth)
		texCoord.x = 0;
	if (texCoord.y < 0 || texCoord.y >= collisionMapHeight)
		texCoord.y = 0;
	return collisionMap.at(texCoord.y).at(texCoord.x);
}

Vector3 Physics::getColourAtPosition(Vector3 position) {
	Vector2 texCoord = getCollisionMapCoord(position.x, position.y);
	return getColourAtTexCoord(texCoord);
}

float Physics::getPenetrationDepth(Vector3 pos1, float r1, Vector3 pos2, float r2) {
	float d = (pos1 - pos2).Length();
	float p = r1 + r2 - d;
	return p;
}

void Physics::spawnArrows() {
	int arrowNum = 0;
	for (vector<Raider*>::iterator raiderIterator = raiders.begin() + 1; raiderIterator != raiders.end(); ++raiderIterator) {
		Follower* follower = (Follower*)(*raiderIterator);
		if (follower->getState() == FOLLOWER_FOLLOW_LEADER) { // If the raider is alive, spawn an arrow at its position and rotation
			float rotation = follower->physicsNode.getRotation();

			float x = -(sin(rotation / 57.29577951f));
			float y = cos(rotation / 57.29577951f);

			Vector3 velocity = -Vector3(x, y, 0);
			velocity.Normalise();
			arrows.at(arrowNum)->physicsNode.updateCurrentVelocity(velocity * arrows.at(arrowNum)->physicsNode.getTargetSpeed());
			arrows.at(arrowNum)->physicsNode.updatePosition(follower->physicsNode.getPosition());
			arrows.at(arrowNum)->physicsNode.updateScale(Vector3(5.0f, 5.0f, 5.0f));
			arrows.at(arrowNum)->physicsNode.updateRotation(follower->physicsNode.getRotation());
			arrows.at(arrowNum)->physicsNode.updateCollisionRadius(5.0f);
			arrows.at(arrowNum)->setCollided(false);
			arrows.at(arrowNum)->setTimerToMax();
			if (follower->getHasDamageBuff()) { // If the follower is buffed increase the arrow damage
				arrows.at(arrowNum)->setDamage(arrowDamage * arrowDamageBuff);
			}
			else {
				arrows.at(arrowNum)->setDamage(arrowDamage);
			}
			++arrowNum;
		}
	}
	shootArrows = false;
}

void Physics::rotateRaiderToDragon(Raider* raider) {
	Vector3 raiderToDragon = dragon.physicsNode.getPosition() - raider->physicsNode.getPosition();
	Vector3 raiderNextPosition = raider->physicsNode.getNextPosition(msec);

	float angle = angleBetweenPoints(raider->physicsNode.getXYPosition(), dragon.physicsNode.getXYPosition());
	raider->physicsNode.updateRotation(angle);
}

void Physics::updateRaiderMovementSpeed(Raider* raider) {
	Vector3 raiderCurrentPosition = raider->physicsNode.getXYPosition();
	Vector2 texCoord = getCollisionMapCoord(raiderCurrentPosition.x, raiderCurrentPosition.y);
	Vector3 groundCol = getColourAtTexCoord(texCoord);
	if (groundCol == rubble) {
		raider->physicsNode.updateTargetVelocity(rockSpeedModifier);
	}
	else if (groundCol != rubble) {
		raider->physicsNode.updateTargetVelocity(groundSpeedModifier);
	}
}

void Physics::processRaiderCollision(Entity* r1, Entity* r2) {
	if (r1 != r2) {
		if (overlap(
			r1->physicsNode.getXYPosition(),
			r1->physicsNode.getCollisionRadius(),
			r2->physicsNode.getXYPosition(),
			r2->physicsNode.getCollisionRadius())) {

			vector<Vector3> collis = collision(
				r1->physicsNode.getCurrentVelocity(),
				r1->physicsNode.getXYPosition(),
				r1->physicsNode.getMass(),
				r2->physicsNode.getCurrentVelocity(),
				r2->physicsNode.getXYPosition(),
				r2->physicsNode.getMass());

			Vector3 r1Pos = r1->physicsNode.getXYPosition();
			Vector3 r2Pos = r2->physicsNode.getXYPosition();
			Vector3 diffVec = r1Pos - r2Pos;
			diffVec.Normalise();
			float penDepth = getPenetrationDepth(r1->physicsNode.getXYPosition(), r1->physicsNode.getCollisionRadius(), r2->physicsNode.getXYPosition(), r2->physicsNode.getCollisionRadius());
			diffVec = diffVec * penDepth;

			/* If r1 or r2 is the leader, don't move that entity, if not, shift the raiders outside each other and update their velocities*/
			if (r1 != (*raiders.begin())) {
				Follower* fol = (Follower*)r1;
				if ((fol->getState() != FOLLOWER_TO_HOARD && fol->getState() != FOLLOWER_FROM_HOARD && fol->getState() != FOLLOWER_TO_POOL && fol->getState() != FOLLOWER_FROM_POOL) || r2 == (*raiders.begin())) { // If r1 is folliwng a path, make it push other followers away
					r1->physicsNode.updateCurrentVelocity(collis.at(0));
					Vector3 newPos = r1Pos + (diffVec / 2);
					Vector3 colAtNewPos = getColourAtPosition(newPos);
					if (colAtNewPos != wall && colAtNewPos != boulder)
						r1->physicsNode.updatePositionNoZ(newPos);
				}
			}
			if (r2 != (*raiders.begin())) {
				Follower* fol = (Follower*)r2;
				if ((fol->getState() != FOLLOWER_TO_HOARD && fol->getState() != FOLLOWER_FROM_HOARD && fol->getState() != FOLLOWER_TO_POOL && fol->getState() != FOLLOWER_FROM_POOL) || r1 == (*raiders.begin())) {// If r2 is folliwng a path, make it push other followers away
					r2->physicsNode.updateCurrentVelocity(collis.at(1));
					Vector3 newPos = r2Pos - (diffVec / 2);
					Vector3 colAtNewPos = getColourAtPosition(newPos);
					if (colAtNewPos != wall && colAtNewPos != boulder)
						r2->physicsNode.updatePositionNoZ(newPos);
				}
			}
		}
	}
}

void Physics::processTerrainCollision(Entity* entity) {
	Vector3 raiderCurrentPosition = entity->physicsNode.getXYPosition();
	Vector3 raiderNextPosition = entity->physicsNode.getPosition() - entity->physicsNode.getCurrentVelocity() * msec;
	Vector3 groundColNextPosition = getColourAtPosition(raiderNextPosition);

	float xOffset = (rand() % 4) - 2;
	float yOffset = (rand() % 4) - 2;

	/* If the entities next position is a wall or boulder, do a collision with its current position and next position. Next position is slightly offset randomly to help the entity not get stuck */
	if (groundColNextPosition == wall || groundColNextPosition == boulder) {
		vector<Vector3> cols = collision(
			entity->physicsNode.getCurrentVelocity(),
			raiderCurrentPosition,
			entity->physicsNode.getMass(),
			Vector3(0, 0, 0),
			Vector3(raiderNextPosition.x + xOffset, raiderNextPosition.y + yOffset, 0.0),
			999999);
		entity->physicsNode.updateCurrentVelocity(cols.at(0));
	}
}

void Physics::moveEntityByVelocity(Entity* entity) {
	/* Update the position of an entity based on its velocity and time since last update */
	entity->physicsNode.updatePositionNoZ(entity->physicsNode.getPosition() - entity->physicsNode.getCurrentVelocity() * msec);
}

void Physics::processBreathCollision(Raider* raider) {

	float x = sin(dragon.physicsNode.getRotation() / 57.29577951f);
	float y = cos(dragon.physicsNode.getRotation() / 57.29577951f);

	Vector3 dragonPos = dragon.physicsNode.getXYPosition();
	Vector3 breathPos = breath.physicsNode.getXYPosition();

	Vector3 breathOrigin = dragonPos + (breathPos * Vector3(x,y,0));
	float breathAngle = 45;

	/* Do a sphere-sphere check between the raider and the dragon, with the dragon sphere radius proportional to the breath x scale */
	if (overlap(raider->physicsNode.getXYPosition(), raider->physicsNode.getCollisionRadius(), dragon.physicsNode.getXYPosition(), breath.physicsNode.getScale().x * 1.5)) {
		float angleBreathToRaider = angleBetweenPoints(raider->physicsNode.getXYPosition(), dragonPos);
		if (angleBreathToRaider < 0)
			angleBreathToRaider += 360;
		float upperAngle = angleBreathToRaider + breathHalfAngle;
		float lowerAngle = angleBreathToRaider - breathHalfAngle;
		float dragonRotation = dragon.physicsNode.getRotation();

		if (dragonRotation < breathHalfAngle) {
			if (upperAngle > 180) {
				upperAngle -= 360;
				lowerAngle -= 360;
			}
		}
		else if (dragonRotation > (360 - breathHalfAngle)) {
			if (upperAngle < 180) {
				upperAngle += 360;
				lowerAngle += 360;
			}
		}
		/* If the rotation of the dragon is within a suitable range of the angle to the follower, deal damage */
		if (dragonRotation < upperAngle && dragonRotation > lowerAngle) {
			raider->setHealth(raider->getHealth() - (breathDamagePerMilli * msec));
		}

	}

	
}

void Physics::spawnRaiders() {
	int offset = 0;
	int row = 0;
	for (vector<Raider*>::iterator i = raiders.begin(); i != raiders.end(); ++i) { // Spawn raiders in columns of size 6
		(*i)->setAlive(true);
		(*i)->physicsNode.updateScale(Vector3(10, 10, 10));
		(*i)->physicsNode.updatePosition(MapCoordinate(33 + offset, 4 - row, 2 + offset));
		++offset;
		if (offset == 6) {
			++row;
			offset = 0;
		}
	}
}

void Physics::updateArrows() {
	for (vector<Arrow*>::iterator arrIt = arrows.begin(); arrIt != arrows.end(); ++arrIt) {
		if (!(*arrIt)->hasCollided()) { // If the arrow has not collided, move it forwards
			moveEntityByVelocity((*arrIt));
			/* Reduce its lifetime left */
			(*arrIt)->reduceTimerLeft(msec);

			if ((*arrIt)->getTimeLeft() <= 0) { // If it runs out of time, destroy it
				(*arrIt)->setCollided(true);
				(*arrIt)->physicsNode.updateScale(Vector3(0, 0, 0));
			}
			else if (overlap((*arrIt)->physicsNode.getXYPosition(),
				(*arrIt)->physicsNode.getCollisionRadius(),
				dragon.physicsNode.getXYPosition(),
				dragon.physicsNode.getCollisionRadius())) { // If it collides with the dragon, deal damage
				dragon.setHealth(dragon.getHealth() - (*arrIt)->getDamage());
				(*arrIt)->setCollided(true);
				(*arrIt)->physicsNode.updateScale(Vector3(0, 0, 0));
			}
			else if (getColourAtPosition((*arrIt)->physicsNode.getPosition()) == wall) { // If it collides with a wall, destroy it
				(*arrIt)->setCollided(true);
				(*arrIt)->physicsNode.updateScale(Vector3(0, 0, 0));
			}
		}
	}
}

void Physics::updateDragon() {
	if(dragon.getState() != DRAGON_DEAD && dragon.getState() != DRAGON_WAIT) {
		updateBreath();
		float raidAliveDragCount = 0;

		if (!dragon.hasTarget()) { //If the dragon does not have a target, find one
			float minDistance = 0;
			Raider* minRaider = raiders.at(1);
			for (vector<Raider*>::iterator i = raiders.begin(); i != raiders.end(); ++i) { // Find the closest raider that is alive
				if ((*i)->isAlive()) {
					++raidAliveDragCount;
					Vector3 dragPos = dragon.physicsNode.getXYPosition();
					Vector3 radPos = (*i)->physicsNode.getXYPosition();
					float thisDistance = (dragPos - radPos).Length();
					if (thisDistance < minDistance || minDistance == 0) {
						minDistance = thisDistance;
						minRaider = (*i);
					}
				}
			}
			dragon.setHasTarget(true);
			dragon.setTarget(minRaider); //Set the closest raider at the target
		}
		Vector3 minRaiderPos = dragon.getTarget()->physicsNode.getXYPosition();
		Vector3 dragPos = dragon.physicsNode.getXYPosition();
		Vector3 minRaiderDist = minRaiderPos - dragPos;
		if (dragon.hasTarget() && minRaiderDist.Length() < dragon.physicsNode.getAggroRange() && ((Raider*)dragon.getTarget())->isAlive()) { // If the dragon has a target that is within its aggro range, rotate towards it, else set no target
			dragon.physicsNode.updateRotation(angleBetweenPoints(dragon.getTarget()->physicsNode.getXYPosition(), dragon.physicsNode.getXYPosition()));
		}
		else {
			dragon.setHasTarget(false);
		}

		if (raidersAlive != 0) {
			float dragMoveAngle = 0;
			if (dragon.getState() == DRAGON_FOLLOW_LEADER) { // If the leader is alive, set dragons movement angle to be towards it
				dragMoveAngle = angleBetweenPoints(raiders.at(0)->physicsNode.getPosition(), dragon.physicsNode.getPosition()) / 57.29577951f;
			}
			else if (dragon.getState() == DRAGON_FOLLOW_TARGET) { // If the leader is dead, set dragons movement angle to be towards the dragons target
				dragMoveAngle = angleBetweenPoints(dragon.getTarget()->physicsNode.getXYPosition(), dragon.physicsNode.getXYPosition()) / 57.29577951f;
			}

			float dragX = -(0.0168 * msec * sin(dragMoveAngle));
			float dragY = 0.0168 * msec * cos(dragMoveAngle);

			Vector3 accel = Vector3(dragX, dragY, 0);
			accel.Normalise();

			dragon.physicsNode.updateCurrentVelocity(dragon.physicsNode.getCurrentVelocity() + (accel * dragonAccelForce * msec));

			limitEntityVelocity(&dragon);

			processTerrainCollision(&dragon);

			if(getColourAtPosition(dragon.physicsNode.getNextXYPosition(msec)) != wall || getColourAtPosition(dragon.physicsNode.getNextXYPosition(msec)) != boulder) // If the dragons next position is not within a wall or boulder, move it
				moveEntityByVelocity(&dragon);
		}

		if (dragon.getHealth() <= 0) {
			dragon.setAlive(false);
			dragon.setState(DRAGON_DEAD);
			((Leader*)raiders.at(0))->setState(LEADER_WAIT);
			for (vector<Raider*>::iterator i = raiders.begin() + 1; i != raiders.end(); ++i) {
				Follower* follower = (Follower*)(*i);
				follower->setState(FOLLOWER_WAIT);
			}
		}
	}
	else { // If the dragon is dead, hide the breath
		breath.physicsNode.updateScale(Vector3(0, 0, 0));
	}
}

void Physics::updateBreath() {
	if (dragon.hasTarget()) { // If the dragon has a target, expand and contract breath, else hide it
		if (breath.physicsNode.getScale().x > breathMaxScale)
		{
			breathState = 0;
			breath.physicsNode.updateScale(Vector3(breathMaxScale, breathMaxScale / 2.0f, 0));
		}
		else if (breath.physicsNode.getScale().x < breathMinScale)
		{
			breathState = 1;
			breath.physicsNode.updateScale(Vector3(breathMinScale, breathMinScale / 2.0f, 0));
		}

		if (breathState == 1)
		{
			breath.physicsNode.updateScaleX(breath.physicsNode.getScale().x + 0.15*msec);
			breath.physicsNode.updateScaleY(breath.physicsNode.getScale().y + 0.075*msec);
		}
		else
		{
			breath.physicsNode.updateScaleX(breath.physicsNode.getScale().x - 0.2 * msec);
			breath.physicsNode.updateScaleY(breath.physicsNode.getScale().y - 0.1 * msec);
		}
	}
	else {
		breath.physicsNode.updateScale(Vector3(0, 0, 0));
	}
}

void Physics::updateFollowers() {
	for (vector<Raider*>::iterator raiderIterator = raiders.begin() + 1; raiderIterator != raiders.end(); ++raiderIterator) {
		Follower* follower = (Follower*)(*raiderIterator);
		if (follower->getState() != FOLLOWER_DEAD && follower->getState() != FOLLOWER_WAIT) {

			processBreathCollision(follower);

			/* Rotate raider to face dragon */
			rotateRaiderToDragon(follower);

			/* Terrain movement speed check */
			updateRaiderMovementSpeed(follower);

			Vector3 raiderNextPosition = follower->physicsNode.getNextXYPosition(msec);

			/* Push colliding raiders away */
			for (vector<Raider*>::iterator i = raiders.begin(); i != raiders.end(); ++i) {
				processRaiderCollision(follower, (*i));
			}
			processRaiderCollision(follower, &dragon);

			/* Terrain Collision */
			processTerrainCollision(follower);

			/* Move if next position isn't a boulder or wall */
			Vector3 colourAtNextPosition = getColourAtPosition(follower->physicsNode.getPosition() - follower->physicsNode.getCurrentVelocity() * msec);
			if (colourAtNextPosition != wall && colourAtNextPosition != boulder)
				moveEntityByVelocity(follower);

			/* Move raider towards leader */
			if (follower->getState() == FOLLOWER_FOLLOW_LEADER) { // If the leader is alive, move towards it, else...
				accelerateFollowerToPosition(follower, raiders.at(0)->physicsNode.getPosition());
			}
			else if (follower->getState() == FOLLOWER_PANIC) {  // ...if a wall or boulder is hit move in a random direction, if the entrance is hit, depsawn
				follower->physicsNode.updateCurrentVelocity(follower->physicsNode.getCurrentVelocity() + follower->physicsNode.getCurrentAcceleration());

				limitEntityVelocity(follower);
				Vector3 groundColNextPos = getColourAtPosition(follower->physicsNode.getNextXYPosition(msec));
				if (groundColNextPos == wall || groundColNextPos == boulder) {
					float randX = (rand() % 10) - 5;
					float randY = (rand() % 10) - 5;
					Vector3 accel(randX, randY, 0);
					accel.Normalise();
					follower->physicsNode.updateCurrentAcceleration(accel * followerAccelForce * msec);
				}
				else if (groundColNextPos == entrance) {
					follower->setHealth(0);
				}
			}
			else if (follower->getState() == FOLLOWER_TO_POOL) { // Follow the path to the pool
				Vector3 followerPos = follower->physicsNode.getXYPosition();
				if ((abs((followerPos - follower->getPath().at(follower->getTargetNode())->getPosition()).Length())) < aStarNodeDistance) { // If the target node has been reached, set next node as the target
					follower->setTargetNode(follower->getTargetNode() - 1);
				}
				if (follower->getTargetNode() >= 0) { // If not at the end of the path, move to the node, else change state
					accelerateFollowerToPosition(follower, follower->getPath().at(follower->getTargetNode())->getPosition());
				}
				else { // Return from the pool and gain hp
					follower->setState(FOLLOWER_FROM_POOL);
					follower->setTargetNode(0);
					follower->setHealth(follower->getMaxHealth());
				}
			}
			else if (follower->getState() == FOLLOWER_FROM_POOL) { // Follow the path from the pool
				Vector3 followerPos = follower->physicsNode.getXYPosition();
				if ((abs((followerPos - follower->getPath().at(follower->getTargetNode())->getPosition()).Length())) < aStarNodeDistance) { // If the target node has been reached, set next node as the target
					follower->setTargetNode(follower->getTargetNode() + 1);
				}
				if (follower->getTargetNode() < follower->getPath().size()) { // If not at the end of the path, move to the node, else change state
					accelerateFollowerToPosition(follower, follower->getPath().at(follower->getTargetNode())->getPosition());
				}
				else {
					follower->setState(FOLLOWER_FOLLOW_LEADER);
				}
			}
			else if (follower->getState() == FOLLOWER_TO_HOARD) { // Follow the path to the hoard
				Vector3 followerPos = follower->physicsNode.getXYPosition();
				if ((abs((followerPos - follower->getPath().at(follower->getTargetNode())->getPosition()).Length())) < aStarNodeDistance) { // If the target node has been reached, set next node as the target
					follower->setTargetNode(follower->getTargetNode() - 1);
				}
				if (follower->getTargetNode() >= 0) { // If not at the end of the path, move to the node, else change state
					accelerateFollowerToPosition(follower, follower->getPath().at(follower->getTargetNode())->getPosition());
				}
				else { // Return from the hoard and gain the buff
					follower->setState(FOLLOWER_FROM_HOARD);
					follower->setTargetNode(0);
					follower->setHasDamageBuff(true);
				}
			}
			else if (follower->getState() == FOLLOWER_FROM_HOARD) { // Follow the path from the hoard
				Vector3 followerPos = follower->physicsNode.getXYPosition();
				if ((abs((followerPos - follower->getPath().at(follower->getTargetNode())->getPosition()).Length())) < aStarNodeDistance) { // If the target node has been reached, set next node as the target
					follower->setTargetNode(follower->getTargetNode() + 1);
				}
				if (follower->getTargetNode() < follower->getPath().size()) { // If not at the end of the path, move to the node, else change state
					accelerateFollowerToPosition(follower, follower->getPath().at(follower->getTargetNode())->getPosition());
				}
				else {
					follower->setState(FOLLOWER_FOLLOW_LEADER);
				}
			}

			if (follower->getHealth() <= 0 && follower->getState() != FOLLOWER_DEAD) {
				follower->setAlive(false);
				follower->physicsNode.updateCollisionRadius(0);
				follower->setState(FOLLOWER_DEAD);
				--raidersAlive;
			}
		}
	}
}

void Physics::updateLeader() {
	/* Leader stuff */
	Leader* leader = (Leader*)raiders.at(0);
	if (leader->getState() != LEADER_DEAD && leader->getState() != LEADER_WAIT) {

		if (arrowCooldown > 0) { // If arrows are on cooldown, reduce the cooldown
			arrowCooldown -= msec;
		}
		if (shootArrows) {
			if (arrowCooldown <= 0) { // If arrows are off cooldown, fire them
				arrowCooldown = arrowCooldownMax;
				spawnArrows();
			}
		}

		processRaiderCollision(leader, &dragon);

		processBreathCollision(leader);

		if (rotateLeaderLeft) {
			leader->physicsNode.updateRotation(leader->physicsNode.getRotation() + (0.2 * msec));
			rotateLeaderLeft = false;
		}
		if (rotateLeaderRight) {
			leader->physicsNode.updateRotation(leader->physicsNode.getRotation() - (0.2 * msec));
			rotateLeaderRight = false;
		}

		if (moveLeader) {
			float x = -(0.028 * msec * sin(raiders.at(0)->physicsNode.getRotation() / 57.29577951f));
			float y = 0.028 * msec * cos(raiders.at(0)->physicsNode.getRotation() / 57.29577951f);

			Vector3 velocity = Vector3(x, y, 0);
			velocity.Normalise();
			velocity = -velocity * raiders.at(0)->physicsNode.getTargetSpeed();
			Vector3 nextCol = getColourAtPosition(leader->physicsNode.getNextPosition(msec));
			leader->physicsNode.updateCurrentVelocity(velocity);

			if (nextCol == rubble) {
				leader->physicsNode.updateTargetVelocity(rockSpeedModifier);
			}
			else if (nextCol != rubble) {
				leader->physicsNode.updateTargetVelocity(groundSpeedModifier);
			}

			if (nextCol != wall && nextCol != boulder) { // Move the leader if its next position is not within a wall or boulder
				moveEntityByVelocity(leader);
			}
			moveLeader = false;
		}

		if (leader->getHealth() <= 0 && leader->getState() != LEADER_DEAD) { // If the leader is dead, set a random vector for each raider to move in
			leader->setState(LEADER_DEAD);
			leader->setAlive(false);
			leader->physicsNode.updateCollisionRadius(0);
			--raidersAlive;
			for (vector<Raider*>::iterator i = raiders.begin() + 1; i != raiders.end(); ++i) {
				Follower* follower = (Follower*)(*i);
				if (follower->getState() != FOLLOWER_DEAD) {
					float randX = (rand() % 10) - 5;
					float randY = (rand() % 10) - 5;
					Vector3 accel(randX, randY, 0);
					accel.Normalise();
					follower->physicsNode.updateCurrentAcceleration(accel * leaderAccelForce * msec);
					follower->setState(FOLLOWER_PANIC);
				}
			}
			dragon.setState(DRAGON_FOLLOW_TARGET);
		}
	}
}

void Physics::limitEntityVelocity(Entity* entity) {
	if (entity->physicsNode.getCurrentVelocity().Length() > entity->physicsNode.getTargetSpeed()) { // If the entities velocity is greater than its current limit, reduce it to the limit
		Vector3 currentV(entity->physicsNode.getCurrentVelocity());
		currentV.Normalise();
		entity->physicsNode.updateCurrentVelocity(currentV * entity->physicsNode.getTargetSpeed());
	}
}

void Physics::accelerateFollowerToPosition(Follower* follower, Vector3 position) {
	Vector3 followerPos = follower->physicsNode.getXYPosition();
	Vector3 targetPos = Vector3(position.x, position.y, 0);

	float angleToPoint = angleBetweenPoints(targetPos, followerPos) / 57.29577951f;

	float raidX = -sin(angleToPoint);
	float raidY = cos(angleToPoint);

	Vector3 accelTowardsPoint = Vector3(raidX, raidY, 0);
	accelTowardsPoint.Normalise();

	follower->physicsNode.updateCurrentVelocity(follower->physicsNode.getCurrentVelocity() + (accelTowardsPoint * followerAccelForce * msec));

	limitEntityVelocity(follower);
}