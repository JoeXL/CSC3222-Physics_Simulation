#include "AStarMap.h"



AStarMap::AStarMap()
{
}

void AStarMap::generateMap(vector<vector<Vector3>> collisionMap, int width, int height) {
	/* Generate nodes */
	for (int x = 0; x < nodesWidth; ++x) {
		vector<AStarNode*> temp;
		nodes.push_back(temp);
		for (int y = 0; y < nodesHeight; ++y) {
			AStarNode* node = new AStarNode;
			MapCoordinate coord = MapCoordinate(x, y, 0);
			node->setPosition(Vector3(coord.getX(), coord.getY(), 0));

			node->setXId(x);
			node->setYId(y);

			int texCoordx = (node->getPosition().x + 864.0) / (1728.0 / (float)width);
			int texCoordy = (-node->getPosition().y + 640.0) / (1078.0 / (float)height);

			if (texCoordx < 0 || texCoordx >= width)
				texCoordx = 0;
			if (texCoordy < 0 || texCoordy >= height)
				texCoordy = 0;

			Vector3 col = collisionMap.at(texCoordy).at(texCoordx);
			if (col == Vector3(1, 0, 0)) {
				node->setTerrainType(HOARD);
			}
			else if (col == Vector3(1, 1, 0)) {
				node->setTerrainType(POOL);
			}
			else if (col == Vector3(0, 1, 1)) {
				node->setTerrainType(BOULDER);
			}
			else if (col == Vector3(1, 0, 1)) {
				node->setTerrainType(RUBBLE);
			}
			else if (col == Vector3(0, 0, 1)) {
				node->setTerrainType(ENTRANCE);
			}
			else if (col == Vector3(0, 1, 0)) {
				node->setTerrainType(WALL);
			}
			else if (col == Vector3(1, 1, 1)) {
				node->setTerrainType(GROUND);
			}
			nodes.at(x).push_back(node);
		}
	}

	/* Populate neighbours of each node. BOULDER and WALL nodes cannot be neighbours */
	for (int i = 0; i < nodes.size(); ++i) {
		for (int j = 0; j < nodes.at(i).size(); ++j) {
			if (i != 0) {
				AStarNode* neighbour = nodes.at(i - 1).at(j);
				if (neighbour->getTerrainType() != BOULDER && neighbour->getTerrainType() != WALL) {
					nodes.at(i).at(j)->addNeighbour(neighbour);
				}
			}
			if (i != nodesWidth - 1) {
				AStarNode* neighbour = nodes.at(i + 1).at(j);
				if (neighbour->getTerrainType() != BOULDER && neighbour->getTerrainType() != WALL) {
					nodes.at(i).at(j)->addNeighbour(neighbour);
				}
			}
			if (j != 0) {
				AStarNode* neighbour = nodes.at(i).at(j - 1);
				if (neighbour->getTerrainType() != BOULDER && neighbour->getTerrainType() != WALL) {
					nodes.at(i).at(j)->addNeighbour(neighbour);
				}
			}
			if (j != nodesHeight - 1) {
				AStarNode* neighbour = nodes.at(i).at(j + 1);
				if (neighbour->getTerrainType() != BOULDER && neighbour->getTerrainType() != WALL) {
					nodes.at(i).at(j)->addNeighbour(neighbour);
				}
			}
		}
	}
}

AStarMap::~AStarMap()
{
	for (int i = 0; i < nodes.size(); ++i) {
		for (int j = 0; j < nodes.at(i).size(); ++j) {
			delete nodes.at(i).at(j);
		}
		nodes.at(i).clear();
	}
	nodes.clear();
}

vector<AStarNode*> AStarMap::calculatePath(Vector2 startPosition, Vector2 endPosition) {
	/* Find the cloest nodes of the start and end positions */
	AStarNode* startNode = findClosestPathableNode(startPosition);
	AStarNode* endNode = findClosestPathableNode(endPosition);
	if (startNode && endNode) {
		/* If the start or end node is a boulder or wall, do not generate a path */
		if (startNode->getTerrainType() != WALL && startNode->getTerrainType() != BOULDER && endNode->getTerrainType() != WALL && endNode->getTerrainType() != BOULDER) {

			vector<AStarNode*> closedSet;

			vector<AStarNode*> openSet;
			openSet.push_back(startNode);

			map<AStarNode*, AStarNode*> cameFrom;

			map <AStarNode*, float> gScore;
			gScore.insert(make_pair(startNode, 0));
			for (int i = 0; i < nodes.size(); ++i) {
				for (int j = 0; j < nodes.at(i).size(); ++j) {
					gScore.insert(make_pair(nodes.at(i).at(j), 999999999));
				}
			}
			gScore.find(startNode)->second = 0;

			map<AStarNode*, float> fScore;
			for (int i = 0; i < nodes.size(); ++i) {
				for (int j = 0; j < nodes.at(i).size(); ++j) {
					fScore.insert(make_pair(nodes.at(i).at(j), 999999999));
				}
			}
			fScore.find(startNode)->second = estimateHeuristic(startNode, endNode);

			while (openSet.size() != 0) {
				/* Set current to be the lowest fScore that is in the openSet */
				AStarNode* current = fScore.begin()->first;
				float lowestFScore = -1;
				for (map<AStarNode*, float>::iterator i = fScore.begin(); i != fScore.end(); ++i) {
					bool inOpenSet = false;
					for (vector<AStarNode*>::iterator it = openSet.begin(); it != openSet.end(); ++it) {
						if ((*i).first == (*it)) {
							inOpenSet = true;
							break;
						}
					}
					if (inOpenSet) {
						if (lowestFScore == -1) {
							current = (*i).first;
							lowestFScore = (*i).second;
						}
						else if ((*i).second < lowestFScore) {
							current = (*i).first;
							lowestFScore = (*i).second;
						}
					}
				}

				/* If the end has been reached, generate the path */
				if (current == endNode) {
					return reconstructPath(cameFrom, current);
				}
				/* Remove the current node from the openSet and add it to the closedSet*/
				for (vector<AStarNode*>::iterator i = openSet.begin(); i != openSet.end(); ++i) {
					if ((*i) == current) {
						openSet.erase(i);
						break;
					}
				}
				closedSet.push_back(current);

				/* Check the neighbours of the current node */
				for (int i = 0; i < current->getNeighbours().size(); ++i) {
					/* Check that the neighbour has not already been evaluated */
					AStarNode* neighbour = current->getNeighbours().at(i);
					bool closedSetContains = false;
					for (vector<AStarNode*>::iterator it = closedSet.begin(); it != closedSet.end(); ++it) {
						if ((*it) == neighbour)
							closedSetContains = true;
					}
					if (closedSetContains) {
						continue;
					}
					/* If the neighbour is not in the openSet, add it */
					bool openSetContains = false;
					for (vector<AStarNode*>::iterator it = openSet.begin(); it != openSet.end(); ++it) {
						if ((*it) == neighbour)
							openSetContains = true;
					}
					if (!openSetContains) {
						openSet.push_back(neighbour);
					}

					/* Calculate the cost of the neighbour can compare it. If the new score is larger, don't update */
					int tentative_gScore = gScore.find(current)->second + calcCost(current, neighbour);
					float foundgScore = gScore.find(neighbour)->second;
					if (tentative_gScore >= foundgScore) {
						continue;
					}

					/* Add current to the neighbours cameFrom map, update scores */
					cameFrom.insert(make_pair(neighbour, current));
					gScore.find(neighbour)->second = tentative_gScore;
					fScore.find(neighbour)->second = gScore.find(neighbour)->second + estimateHeuristic(neighbour, endNode);
				}
			}
		}
	}
	/* Return an empty vector if a path wasn't found */
	vector<AStarNode*> foo;
	return foo;
}

vector<AStarNode*> AStarMap::reconstructPath(map<AStarNode*, AStarNode*> cameFrom, AStarNode* current) {
	/* Generate a vector via the cameFrom map */
	vector<AStarNode*> total_path;
	total_path.push_back(current);
	while(cameFrom.find(current) != cameFrom.end()) {
		current = cameFrom.find(current)->second;
		total_path.push_back(current);
	}
	return total_path;
}

float AStarMap::estimateHeuristic(AStarNode* start, AStarNode* end) {
	return abs((start->getPosition() - end->getPosition()).Length());
}

float AStarMap::calcCost(AStarNode* current, AStarNode* neighbour) {
	TerrainType neighbourTerrain = neighbour->getTerrainType();
	if (neighbourTerrain == GROUND || neighbourTerrain == HOARD || neighbourTerrain == POOL || neighbourTerrain == ENTRANCE) {
		return abs((current->getPosition() - neighbour->getPosition()).Length());
	}
	else if (neighbourTerrain == RUBBLE) {
		return abs((current->getPosition() - neighbour->getPosition()).Length()) * rubbleMultiplier; // Cost is higher for rubble
	}
	return 999999999; // For WALLs and BOULDERs, though no nodes have either as neighbours so this shouldn't be called
}

AStarNode* AStarMap::findClosestPathableNode(Vector2 position) { // Find the closest node that is not a WALL or BOULDER
	AStarNode* closestNode = nodes.at(0).at(0);
	float smallestDist = 99999999;

	for (int i = 0; i < nodes.size(); ++i) {
		for (int j = 0; j < nodes.at(i).size(); ++j) {
			AStarNode* currentNode = nodes.at(i).at(j);
			float dist = abs((Vector3(position.x, position.y, 0) - Vector3(currentNode->getPosition().x, currentNode->getPosition().y, 0)).Length());
			if (dist < smallestDist && currentNode->getTerrainType() != WALL && currentNode->getTerrainType() != BOULDER) {
				closestNode = nodes.at(i).at(j);
				smallestDist = dist;
			}
		}
	}

	return closestNode;
}