#pragma once

#include <vector>
#include "../../nclgl/Vector3.h"
#include "../../nclgl/Vector2.h"
#include "../../nclgl/Matrix4.h"
#include "Follower.h"
#include "Leader.h"
#include "Dragon.h"
#include "Arrow.h"
#include "../../nclgl/Keyboard.h"
#include <iostream>
#include "AStarMap.h"
#include <time.h>

using std::vector;

/* An elementary Physics Data structure, containing the bare minimum of information necessary to
say where an object should appear, and which way it should point. As we increase the complexity
of our physics system, we will add elements to this structure, but the elements which can affect
the graphical representation of entities are present below (and integrated with the graphics
framework already, to make your life easier). Ultimately, you may decide to replace this data
structure entirely (for example with a class to allow inclusion of embedded functions) but be
aware that doing so might require some slight doctoring of Renderer::Renderer() and
SceneNode::Update() */

/*struct PhysicsData {
	Vector3 position;
	float rotation;
	Vector3 scale;
};*/

/* Our placeholder physics system includes an update function, and physics data for each entity
in our environment. We also include a single integer to inform the dragon's behaviour and breath
weapon scaling.*/

class Physics {
public:
	Physics();
	~Physics();

	void UpdatePhysics(float msec, Keyboard* keyboard);
	/* Calculate the angle between two points */
	const float angleBetweenPoints(Vector3 lhs, Vector3 rhs) const;
	/* Perform a sphere-sphere collision check */
	const bool overlap(Vector3 lhs, float lhsRadius, Vector3 rhs, float rhsRadius) const;
	/* Returns a vector of size 2 which represent the two output velocities from the collision */
	vector<Vector3> collision(Vector3 iVelocity, Vector3 iPosition, float iMass, Vector3 jVelocity, Vector3 jPosition, float jMass);

	/* Translate from a position in space to a texture coordinate of the collision map */
	Vector2 getCollisionMapCoord(float x, float y);
	/* Get the colour of the pixel at the given texture coordinate */
	Vector3 getColourAtTexCoord(Vector2 texCoord);
	/* Get the colour of the pixel at the given world position */
	Vector3 getColourAtPosition(Vector3 position);
	/* Calculate the depth two circles penerate each other */
	float getPenetrationDepth(Vector3 pos1, float r1, Vector3 pos2, float r2);

	/* Update raider movement speed based on floor type */
	void updateRaiderMovementSpeed(Raider* raider);
	void rotateRaiderToDragon(Raider* raider);
	void processRaiderCollision(Entity* r1, Entity* r2);
	void processTerrainCollision(Entity* entity);
	void processBreathCollision(Raider* raider);
	void moveEntityByVelocity(Entity* entity);
	void spawnArrows();
	void updateArrows();
	void updateDragon();
	void updateBreath();
	void spawnRaiders();
	void updateFollowers();
	void updateLeader();
	/* Reduce the velocity of an entity to its limit */
	void limitEntityVelocity(Entity* entity);
	void accelerateFollowerToPosition(Follower* follower, Vector3 position);

	void processKeys(Keyboard* keyboard);

	Entity map;
	Entity endScreen;
	Dragon dragon;
	Entity breath;
	vector<Raider*> raiders;
	vector<Arrow*> arrows;

	vector<Entity*> nodes;

	Vector2 poolPosition = Vector2(450, 92);
	Vector2 hoardPosition = Vector2(-159, 478);

	vector<vector<Vector3>> collisionMap;
	int collisionMapWidth;
	int collisionMapHeight;

	AStarMap aStarMap;
	
private:
	int raidersAlive;
	int arrowCooldown = 0;
	int numRaiders;
	int	dragonState;
	int breathState;
	bool shootArrows = false;
	bool gameStarted = false;
	bool moveLeader = false;
	bool rotateLeaderLeft = false;
	bool rotateLeaderRight = false;
	float msec;


	float groundSpeedModifier = 0.027f;
	float rockSpeedModifier = 0.0108f;
	float dragonSpeedModifier = 0.0162f;
	float arrowSpeedModifier = 0.081f;

	float leaderMass = 50.0f;
	int leaderHealth = 200;
	int leaderMaxHealth = 200;
	float leaderAccelForce = 0.01f;

	float followerMass = 1.0f;
	int followerHealthMin = 100;
	int followerHealthMax = 150;
	float followerAccelForce = 0.01f;

	int arrowDamage = 10;
	int arrowDamageBuff = 1.5;
	int arrowLifetime = 2000;
	int arrowCooldownMax = 6000;

	int dragonAggroRange = 8;
	float dragonMass = 5000.0f;
	int dragonHealth = 1000;
	int dragonMaxHealth = 1000;
	float dragonAccelForce = 0.01f;

	float breathMaxScale = 100.0f;
	float breathMinScale = 2.0f;
	float breathDamagePerMilli = 0.05;

	float breathHalfAngle = 20.0f;

	bool showAStarNodeMap = false;
	float aStarNodeDistance = 2.0f;

	Vector3 endScreenScale = Vector3(691.2f, 216.0f, 100.0f);

	/* Ground colour values */
	Vector3 hoard = Vector3(1, 0, 0);
	Vector3 pool = Vector3(1, 1, 0);
	Vector3 boulder = Vector3(0, 1, 1);
	Vector3 rubble = Vector3(1, 0, 1);
	Vector3 entrance = Vector3(0, 0, 1);
	Vector3 wall = Vector3(0, 1, 0);
	Vector3 ground = Vector3(1, 1, 1);
};