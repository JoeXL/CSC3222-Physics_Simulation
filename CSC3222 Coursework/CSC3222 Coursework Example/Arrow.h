#pragma once
#include "Entity.h"
class Arrow :
	public Entity
{
public:
	Arrow();
	~Arrow();

	void setDamage(int dmg) { damage = dmg; }
	const int& getDamage() const { return damage; }

	void setCollided(bool col) { collided = col; }
	const bool& hasCollided() const { return collided; }

	void setLifetime(int milli) { lifetime = milli; }
	const int& getLifetime() { return lifetime; }

	void setTimerToMax() { timeLeft = lifetime; }
	const int& getTimeLeft() const { return timeLeft; }
	void reduceTimerLeft(int milli) { timeLeft -= milli; }

private:
	int damage;
	bool collided = true;
	int timeLeft = 0;
	int lifetime = 2000;
};

