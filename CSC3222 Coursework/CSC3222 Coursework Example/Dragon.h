#pragma once
#include "Entity.h"

enum StateDragon {
	DRAGON_WAIT, DRAGON_FOLLOW_LEADER, DRAGON_FOLLOW_TARGET, DRAGON_DEAD
};

class Dragon : public Entity
{
public:
	Dragon();
	~Dragon();

	const int& getHealth() const { return health; }
	void setHealth(int hp) { health = hp; }

	const int& getMaxHealth() const { return maxHealth; }
	void setMaxHealth(int hp) { maxHealth = hp; }

	const bool& hasTarget() const { return blHasTarget; }
	void setHasTarget(bool b) { blHasTarget = b; }

	const bool& isAlive() const { return alive; }
	void setAlive(bool b) { alive = b; }

	Entity* getTarget() const { return target; }
	void setTarget(Entity* e) { target = e; }

	const StateDragon& getState() const { return state; }
	void setState(StateDragon s) { state = s; }

private:
	int health;
	int maxHealth;

	bool alive = false;

	bool blHasTarget = false;
	Entity* target = nullptr;

	StateDragon state;
};

